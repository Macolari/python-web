from flask import Flask, render_template
from pymongo import MongoClient
import requests

conn = MongoClient()

app = Flask(__name__)

@app.route('/')
def index():
    var = list(range(10))
    return render_template('base.html', numeros=var)
@app.route('/cep/<string:cep>')
def teste(cep):
    data = requests.get('https://viacep.com.br/ws/{}/json/'.format(cep))
    print(data.json())
    return render_template('cep.html', data=data.json())
if __name__ == '__main__':
    app.run(debug=True)